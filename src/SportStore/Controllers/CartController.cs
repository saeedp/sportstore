﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportStore.Infrastructure;
using SportStore.Models;
using SportStore.Models.ViewModels;
using SportStore.Repository;

namespace SportStore.Controllers
{
    public class CartController : Controller
    {
        private readonly IProductRepository _repository;
        private Cart _cart;

        public CartController(IProductRepository repository, Cart cart)
        {
            _repository = repository;
            _cart = cart;
        }

        public ViewResult Index(string returnUrl)
        {
            var model = GetCartViewModel(returnUrl);

            return View(model);
        }

        private CartIndexViewModel GetCartViewModel(string returnUrl)
        {
            return new CartIndexViewModel
            {
                Cart = _cart,
                ReturnUrl = returnUrl
            };
        }

        public RedirectToActionResult AddToCart(
            int productId, string returnUrl)
        {
            Product product = _repository.Products
                .FirstOrDefault(p => p.ProductID == productId);

            if (product != null)
            {
                _cart.AddItem(product, 1);
                SaveCart(_cart);
            }

            return RedirectToAction("Index", new {returnUrl});
        }

        public RedirectToActionResult RemoveFromCart(int productId,
            string returnUrl)
        {
            Product product = _repository
                .Products.FirstOrDefault(p => p.ProductID == productId);

            if (product != null)
            {
                _cart.RemoveLine(product);
                SaveCart(_cart);
            }

            return RedirectToAction("Index", new {returnUrl});
        }


        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }


    }
}