﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.EntityFrameworkCore;
using SportStore.Data;

namespace SportStore.Models
{
    public class EFOrderRepository : IOrderRepository
    {
        private SportStoreDataContext _context;

        public EFOrderRepository(SportStoreDataContext context)
        {
            _context = context;
        }

        public IQueryable<Order> Orders => _context.Orders
            .Include(o => o.Lines)
            .ThenInclude(l => l.Product);

        public void SaveOrder(Order order)
        {
            _context.AttachRange(order.Lines.Select(x => x.Product));
            if(order.OrderID == 0)
                _context.Orders.Add(order);

            _context.SaveChanges();
        }
    }
}